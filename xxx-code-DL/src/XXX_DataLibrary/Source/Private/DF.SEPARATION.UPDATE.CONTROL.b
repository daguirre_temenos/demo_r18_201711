    PROGRAM DF.SEPARATION.UPDATE.CONTROL
*====================================================
* Program to take list of files and update
* F.FILE.CONTROL with date field values
* Entries are delivered in DF_SEPARATION/config 
* in file 'ControlFields'
*
*****************************************************
*                     Inserts
*****************************************************
    $INSERT I_F.FILE.CONTROL
    $INSERT I_F.SPF
    INCLUDE JBC.h
*****************************************************
    GOSUB INITIALISATION
    GOSUB OPEN.FILES
    GOSUB UPDATE.FILE.CONTROL
    STOP

INITIALISATION:
******************************************************
* Initialise all the variables
******************************************************
    PROMPT ':'
    Hdr = STR('%',70)
    FCRec = '' ;
    RETURN
*=====================================================
OPEN.FILES:
******************************************************
* Open All Required files
******************************************************
    OPEN 'VOC' TO F.VOC ELSE
        CRT 'Error Opening VOC Table'
        STOP
    END

    OPEN 'F.SPF' TO F.SPF ELSE
        CRT 'Error Opening SPF'
        STOP
    END

    T24RELEASE = ""
    READ SPF.REC FROM F.SPF ,'SYSTEM' THEN
        T24RELEASE = SPF.REC<SPF.CURRENT.RELEASE>
    END


**************************************************
* Date Fields to be included for processing is
* Introduced in File Control So need to open
* File Control for processing
**************************************************
    OPEN 'F.FILE.CONTROL' TO F.FILE.CONTROL ELSE
        CRT 'Error Opening File Control'
        STOP
    END

    OPEN 'DF_SEPARATION/config' TO F.SeparationConfig ELSE
        CRT 'Error Opening DF_SEPARATION/config'
        STOP
    END

    RETURN

*******************************************************
* Read 'ControlFields' from DF_SEPARATION/config and
* update F.FILE.CONTROL
*******************************************************

UPDATE.FILE.CONTROL:

    READ ctrlList FROM F.SeparationConfig, T24RELEASE:'_ControlFields' ELSE ctrlList = ""
    IF ctrlList EQ "" THEN
        READ ctrlList FROM F.SeparationConfig, 'ControlFields' ELSE
            CRT 'Cannot read "ControlFields" from DF_SEPARATION/config directory'
            STOP
        END
    END

    LOOP
        ctrlValues = ctrlList<1>
        DEL ctrlList<1>

    WHILE ctrlValues NE "" DO
        ctrlFile = ctrlValues['$',1,1]
        ctrlFields = ctrlValues['$',2,1]
        GOSUB PROCESS.CONTROL.RECORD    ;* Find list of related files and place in FILE.LIST

    REPEAT

    RETURN

******************************************************
PROCESS.CONTROL.RECORD:


*
* Check if file resident in file control
*
    READ R.FC FROM F.FILE.CONTROL,ctrlFile ELSE
        errorsRec = 'Error ':ctrlFile:' missing F.FILE.CONTROL '
        CRT errorsRec:" - Skipping entry"
        RETURN
    END

*
* Update File Control record
*

    IF R.FC<EB.FILE.DL.DATE.FIELD> EQ "" THEN
        R.FC<EB.FILE.DL.DATE.FIELD> = CHANGE(ctrlFields,'#',@VM)
        CRT "Update F.FILE.CONTROL record '":ctrlFile:"' with separation and archive field definitions"
        WRITE R.FC ON F.FILE.CONTROL,ctrlFile
    END ELSE
        CRT "Skip update to F.FILE.CONTROL record '":ctrlFile:"', already contains data in field ":EB.FILE.DL.DATE.FIELD
    END

    RETURN
END
