    PROGRAM DF.SEPARATION.VOC.UPDATE
*====================================================
* Program to take list of files and update
* VOC with new entries specifing ASSOCIATE or READONLY
* qualifiers 
*
*****************************************************
*                     Inserts
*****************************************************
    INCLUDE JBC.h
*****************************************************
    GOSUB INITIALISATION
    GOSUB OPEN.FILES
    GOSUB PROCESS.VOC.ENTRIES
    STOP

INITIALISATION:
******************************************************
* Initialise all the variables
******************************************************
    PROMPT ':'
    Hdr = STR('%',70)
    FCRec = '' ;
    RETURN
*=====================================================
OPEN.FILES:
******************************************************
* Open All Required files
******************************************************
    OPEN 'VOC' TO F.VOC ELSE
        CRT 'Error Opening VOC Table'
        STOP
    END

    OPEN 'F.SPF' TO F.SPF ELSE
        CRT 'Error Opening SPF'
        STOP
    END

    READ spfRecord FROM F.VOC,'F.SPF' ELSE
	CRT 'Error reading F.SPF record from VOC'
        STOP
    END

    OPEN 'DF_SEPARATION/config' TO F.Conf ELSE
        CRT 'Error Opening DF_SEPARATION/config'
        STOP
    END

    OPEN 'DF_SEPARATION/Files/VOC_Entries' TO F.ROVoc ELSE
        CRT 'Error Opening DF_SEPARATION/Files/VOC_Entries'
        STOP
    END

* Pick up restricted list of tables

    READ restrictedFiles FROM F.Conf,'RestrictedFiles' ELSE restrictedFiles = ""

* Pick up excluded list of tables

    READ excludedFiles FROM F.Conf,'ExcludedFiles' ELSE excludedFiles = ""

* Pick up restricted list of companies

    READ restrictedCompanys FROM F.Conf,'RestrictedCompanys' ELSE restrictedCompanys = ""

* Pick up excluded list of companies

    READ excludedCompanys FROM F.Conf,'ExcludedCompanys' ELSE excludedCompanys = ""

    EXECUTE 'CREATE.FILE DATA DF_SEPARATION/Files/orig_VOC_Entries 1001' CAPTURING Output

    OPEN 'DF_SEPARATION/Files/orig_VOC_Entries' TO ELD.Voc ELSE
        CRT 'Error Opening DF_SEPARATION/Files/orig_VOC_Entries'
        STOP
    END


    RETURN

*******************************************************
* Read entries from DF_SEPARATION/Files/VOC_Entries
* and update VOC
*******************************************************

PROCESS.VOC.ENTRIES:

    CRT "Processing generated VOC entries from DF_SEPARATION/Files/VOCEntries"

    SSELECT F.ROVoc ;* Select sorted list of VOC entries

    LOOP

    WHILE READNEXT vocEntry DO

        READ vocRecord FROM F.ROVoc, vocEntry THEN

            GOSUB PROCESS.VOC.RECORD    ;* Process VOC entry

        END

    REPEAT

    vocEntry = "F.RO.COPY.KEYLIST"
    vocRecord = spfRecord		;* Base entry on F.SPF
    vocRecord<3> = ""			;* No dict
    vocRecord<4> = "F_RO_COPY_KEYLIST NOXMLSCHEMA"
    WRITE vocRecord ON F.VOC, vocEntry

    vocEntry = "F.RO.PURGE.KEYLIST"
    vocRecord<4> = "F_RO_PURGE_KEYLIST NOXMLSCHEMA"
    WRITE vocRecord ON F.VOC, vocEntry

    vocEntry = "F.RO.ERROR.KEYLIST"
    vocRecord<4> = "F_RO_ERROR_KEYLIST NOXMLSCHEMA"
    WRITE vocRecord ON F.VOC, vocEntry

    RETURN

******************************************************
PROCESS.VOC.RECORD:

*
* Check if file resident in....
*

    IF LEN(restrictedCompanys) OR LEN(excludedCompanys) THEN
        vocName = CHANGE(vocEntry,"$RO","")
        GOSUB CHECK.COMPANY.RESTRICTION ;* Restricted companys configured check list
        IF compName EQ "" THEN RETURN   ;* Skip if Company either excluded or not in restricted list
    END

    IF LEN(restrictedFiles) OR LEN(excludedFiles) THEN
        vocName = CHANGE(vocEntry,"$RO","")
        GOSUB CHECK.FILE.RESTRICTION    ;* Restricted files configured check list
        IF restrictedName EQ "" THEN RETURN       ;* Skip if file not found in restricted or found in excluded list
    END

*
* Save original voc entry if not already saved
*
    READ eldRecord FROM ELD.Voc, vocEntry ELSE

*
* Read original entry from current voc
*
        READ eldRecord FROM F.VOC, vocEntry THEN
            WRITE eldRecord ON ELD.Voc,vocEntry ;* Write on eld voc file
        END
    END

*
* Update VOC table with new record
*

    CRT "Update VOC record '":vocEntry:"' with separation qualifiers"
    WRITE vocRecord ON F.VOC, vocEntry

    RETURN

*
* Check if voc entry exists in restricted files or excluded
* input vocName, output restrictedName
*

CHECK.FILE.RESTRICTION:

    restrictedName = vocName

    IF LEN(restrictedFiles) THEN
        FIND restrictedName IN restrictedFiles SETTING restrictedPos THEN
            restrictedName = restrictedFiles<restrictedPos>
        END ELSE
            restrictedName = ""
            RETURN
        END
    END

    FIND restrictedName IN excludedFiles SETTING excludedPos THEN
        restrictedName = ""
        RETURN
    END

    RETURN

*
* Check if voc entry exists in restricted companies or excluded companies
* input vocName, output compName
*

CHECK.COMPANY.RESTRICTION:

    compName = vocName[1,4]

    IF LEN(restrictedCompanys) THEN
        FIND compName IN restrictedCompanys SETTING restrictedPos THEN
            compName = restrictedCompanys<restrictedPos>
        END ELSE
            compName = ""
            RETURN
        END
    END

    FIND compName IN excludedCompanys SETTING excludedPos THEN
        compName = ""
        RETURN
    END

    RETURN

