    PROGRAM DF.SEPARATION.LIST.GEN
*====================================================
* Program to Get the list of files and Dictionaries
* Files need to be split between
* - Volatile - Live Tables
* - Non Volatile - Read Only Tables 
*****************************************************
*                     Inserts
*****************************************************

    $INSERT I_COMMON
    $INSERT I_EQUATE
    $INSERT I_F.ARCHIVE
    $INSERT I_F.COMPANY
    $INSERT I_F.FILE.CONTROL
	$INSERT I_F.SPF
    INCLUDE JBC.h
*****************************************************
    GOSUB INITIALISATION
    GOSUB CREATE.FILES
    GOSUB OPEN.FILES
    GOSUB COMPANY.INFO
    GOSUB ARCH.LIST
    GOSUB COMP.SPLIT
    GOSUB VOL.LIST
    GOSUB SCAN.VOC
    STOP

INITIALISATION:
******************************************************
* Initialise all the variables
******************************************************
    PROMPT ':'
    Hdr = STR('%',70)
    F.VOC = ''; VOC.REC = '' ; F.SPF = '' ; SPF.REC = '' ; FCRec = '' ; DB.TYPE = '' ; Output = ''
    F.MOVE = '' ; VOCID = '' ; CMD = '' ; F.DataTables = '' ; F.DictTables = '';
    F.VolatileDataTables = '' ; F.VolatileDictTables = '' ; F.Errors = ''
    DataFileRec = '' ; DictFileRec = '' ; TempDictRec = '' ; NvolId = '' ; NvolRec = '' ; RelFiles = ''
    CompId = '' ; CompRec = '' ; CompInfo = '' ; TempVolRec = ''
    DB.ORACLE = 0; DB.DB2 = 0 ; DB.MSSQL = 0 ; DB.JBASE = 0 ; I = 0; NVCNT = 0 ; J = 0 ;
    VOL.POS = 0 ; VOL.POS1 = ''

    RETURN
*=====================================================
OPEN.FILES:
******************************************************
* Open All Required files
******************************************************
    OPEN 'VOC' TO F.VOC ELSE
        CRT 'Error Opening VOC Table'
        STOP
    END

    OPEN 'F.SPF' TO F.SPF ELSE
        CRT 'Error Opening SPF'
        STOP
    END

    OPEN 'F.COMPANY' TO F.COMPANY ELSE
        CRT 'Error Opening COMPANY'
        STOP
    END

    OPEN 'F.ARCHIVE' TO F.ARCHIVE ELSE
        CRT 'Error Opening Archive'
        STOP
    END
**************************************************
* Date Fields to be included for processing is
* Introduced in File Control So need to open
* File Control for processing
**************************************************
    OPEN 'F.FILE.CONTROL' TO F.FILE.CONTROL ELSE
        CRT 'Error Opening File Control'
        STOP
    END

    OPEN 'DF_SEPARATION/Files/Errors' TO F.Errors ELSE
        CRT 'Error Opening DF_SEPARATION/Files/Errors'
        STOP
    END

    OPEN 'DF_SEPARATION/Files' TO F.SeparationFiles ELSE
        CRT 'Error Opening DF_SEPARATION/Files'
        STOP
    END

    OPEN 'DF_SEPARATION/Files/DataTables' TO F.DataTables ELSE
        CRT 'Error Opening DF_SEPARATION/Files/DataTables'
        STOP
    END

    OPEN 'DF_SEPARATION/Files/DictTables' TO F.DictTables ELSE
        CRT 'Error Opening DF_SEPARATION/Files/DictTables'
        STOP
    END

    OPEN 'DF_SEPARATION/Files/CompanyNonVolatileTables' TO F.CompanyNonVolatileTables ELSE
        CRT 'Error Opening DF_SEPARATION/Files/CompanyNonVolatileTables'
        STOP
    END

    OPEN 'DF_SEPARATION/Files/ArchiveTables' TO F.ArchiveTables ELSE
        CRT 'Error Opening DF_SEPARATION/Files/ArchiveTables'
        STOP
    END

    OPEN 'DF_SEPARATION/Files/CompanyTables' TO F.CompanyTables ELSE
        CRT 'Error Opening DF_SEPARATION/Files/CompanyTables'
        STOP
    END

    OPEN 'DF_SEPARATION/Files/VolatileDataTables' TO F.VolatileDataTables ELSE
        CRT 'Error Opening DF_SEPARATION/Files/VolatileDataTables'
        STOP
    END

    OPEN 'DF_SEPARATION/Files/VolatileDictTables' TO F.VolatileDictTables ELSE
        CRT 'Error Opening DF_SEPARATION/Files/VolatileDictTables'
        STOP
    END

    OPEN 'DF_SEPARATION/Files/NonVolatileDataTables' TO F.NonVolatileDataTables ELSE
        CRT 'Error Opening DF_SEPARATION/Files/NonVolatileDataTables'
        STOP
    END

    OPEN 'DF_SEPARATION/Files/NonVolatileDictTables' TO F.NonVolatileDictTables ELSE
        CRT 'Error Opening DF_SEPARATION/Files/NonVolatileDictTables'
        STOP
    END

    OPEN 'DF_SEPARATION/Files/ArcTables' TO F.ArcTables ELSE
        CRT 'Error Opening DF_SEPARATION/Files/ArcTables'
        STOP
    END

    OPEN 'DF_SEPARATION/Files/TablePointers' TO F.TablePointers ELSE
        CRT 'Error Opening DF_SEPARATION/Files/TablePointers'
        STOP
    END

    OPEN 'DF_SEPARATION/config' TO F.SeparationConfig ELSE
        CRT 'Error Opening DF_SEPARATION/config'
        STOP
    END
*******************************************************
* Check existing Database Used DB2/ORACLE/MSSQL
*******************************************************
    STATUS SPF.REC FROM F.SPF ELSE SPF.REC = ''
    DB.TYPE=SPF.REC<21>

*    BEGIN CASE
*    CASE DB.TYPE='XMLORACLE'
*        DB.ORACLE = 1
*    CASE DB.TYPE='XMLDB2'
*        DB.DB2 = 1
*    CASE DB.TYPE='XMLMSSQL'
*        DB.MSSQL = 1
*    CASE 1
*        DB.JBASE = 1
*    END CASE
    CRT ''
    CRT 'Current installation configured for :':DB.TYPE[4,LEN(DB.TYPE)]
    CRT ''
    RETURN
*=====================================================
CREATE.FILES:
******************************************************
* Create All required folders and files to store
* the information gathered from the program
******************************************************

    EXECUTE 'DELETE-FILE DATA DF_SEPARATION/Files' CAPTURING Output
    EXECUTE 'CREATE.FILE DATA DF_SEPARATION/Files TYPE=UD' CAPTURING Output

    EXECUTE 'DELETE-FILE DATA DF_SEPARATION/Files/Errors' CAPTURING Output
    EXECUTE 'CREATE.FILE DATA DF_SEPARATION/Files/Errors 10001' CAPTURING Output

    EXECUTE 'DELETE-FILE DATA DF_SEPARATION/Files/DataTables' CAPTURING Output
    EXECUTE 'CREATE.FILE DATA DF_SEPARATION/Files/DataTables 10001' CAPTURING Output

    EXECUTE 'DELETE-FILE DATA DF_SEPARATION/Files/DictTables' CAPTURING Output
    EXECUTE 'CREATE.FILE DATA DF_SEPARATION/Files/DictTables 10001' CAPTURING Output

    EXECUTE 'DELETE-FILE DATA DF_SEPARATION/Files/CompanyNonVolatileTables' CAPTURING Output
    EXECUTE 'CREATE.FILE DATA DF_SEPARATION/Files/CompanyNonVolatileTables 10001' CAPTURING Output

    EXECUTE 'DELETE-FILE DATA DF_SEPARATION/Files/ArchiveTables' CAPTURING Output
    EXECUTE 'CREATE.FILE DATA DF_SEPARATION/Files/ArchiveTables 10001' CAPTURING Output

    EXECUTE 'DELETE-FILE DATA DF_SEPARATION/Files/CompanyTables' CAPTURING Output
    EXECUTE 'CREATE.FILE DATA DF_SEPARATION/Files/CompanyTables 10001' CAPTURING Output

    EXECUTE 'DELETE-FILE DATA DF_SEPARATION/Files/VolatileDataTables' CAPTURING Output
    EXECUTE 'CREATE.FILE DATA DF_SEPARATION/Files/VolatileDataTables 10001' CAPTURING Output

    EXECUTE 'DELETE-FILE DATA DF_SEPARATION/Files/VolatileDictTables' CAPTURING Output
    EXECUTE 'CREATE.FILE DATA DF_SEPARATION/Files/VolatileDictTables 10001' CAPTURING Output

    EXECUTE 'DELETE-FILE DATA DF_SEPARATION/Files/NonVolatileDataTables' CAPTURING Output
    EXECUTE 'CREATE.FILE DATA DF_SEPARATION/Files/NonVolatileDataTables 10001' CAPTURING Output

    EXECUTE 'DELETE-FILE DATA DF_SEPARATION/Files/NonVolatileDictTables' CAPTURING Output
    EXECUTE 'CREATE.FILE DATA DF_SEPARATION/Files/NonVolatileDictTables 10001' CAPTURING Output

    EXECUTE 'DELETE-FILE DATA DF_SEPARATION/Files/ArcTables' CAPTURING Output
    EXECUTE 'CREATE.FILE DATA DF_SEPARATION/Files/ArcTables 10001' CAPTURING Output

    EXECUTE 'DELETE-FILE DATA DF_SEPARATION/Files/TablePointers' CAPTURING Output
    EXECUTE 'CREATE.FILE DATA DF_SEPARATION/Files/TablePointers 10001' CAPTURING Output

    EXECUTE 'DELETE-FILE DATA DF_SEPARATION/Scripts' CAPTURING Output
    EXECUTE 'CREATE.FILE DATA DF_SEPARATION/Scripts TYPE=UD'CAPTURING Output

    EXECUTE 'DELETE-FILE DATA DF_SEPARATION/Files/VOC_Entries' CAPTURING Output
    EXECUTE 'CREATE.FILE DATA DF_SEPARATION/Files/VOC_Entries 10001' CAPTURING Output

    CRT ''
    CRT Hdr
    CRT '     Database Separation Table Utility '
    CRT Hdr
    CRT ''
    CRT 'Creating DF_SEPARATION/Files'
    CRT 'Creating DF_SEPARATION/Files/DataTables'
    CRT 'Creating DF_SEPARATION/Files/DictTables'
    CRT 'Creating DF_SEPARATION/Files/CompanyNonVolatileTables'
    CRT 'Creating DF_SEPARATION/Files/ArchiveTables'
    CRT 'Creating DF_SEPARATION/Files/CompanyTables'
    CRT 'Creating DF_SEPARATION/Files/VolatileDataTables'
    CRT 'Creating DF_SEPARATION/Files/VolatileDictTables'
    CRT 'Creating DF_SEPARATION/Files/NonVolatileDataTables'
    CRT 'Creating DF_SEPARATION/Files/NonVolatileDictTables'
    CRT 'Creating DF_SEPARATION/Files/ArcTables'
    CRT 'Creating DF_SEPARATION/Files/TablePointers'
    CRT 'Creating DF_SEPARATION/Files/VOC_Entries'
    CRT 'Creating DF_SEPARATION/Scripts'
    CRT ''


    RETURN
*=====================================================
SCAN.VOC:
*******************************************************
* Scan the VOC table to get the list of valid T24 Files
* Write two list one for the DATA part and one for
* the DICT Part
*******************************************************
    X = '' ; XX = '' ; CompCnt = ''
    CompRelFiles = '' ; TempRec = ''
    CompCnt = DCOUNT(CompInfo,@FM)

    EXECUTE 'CLEARSELECT' CAPTURING Output
    CRT 'Scanning VOC for Volatile and Non-Volatile [Data & Dict] Tables'
    CRT '----------------------------------------------------------------'
    CMD :="SELECT VOC WITH *A1 EQ 'TABLE' AND WITH (*A3 NE '' AND UNLIKE "
    CMD :='"...dict...") AND WITH (*A4 NE '
    CMD :="'' AND UNLIKE "
    CMD :='"...data...")'
    CMD :="AND WITH (*A3 NE '' AND UNLIKE "
    CMD :='"...interface...")'

    CRT 'Selecting VOC'
    EXECUTE CMD CAPTURING Output
    CNT = @SELECTED
    CRT ''
    LOOP
        READNEXT VOCID ELSE VOCID = ''
    WHILE VOCID
        I+=1
        READ VOC.REC FROM F.VOC,VOCID ELSE
            CRT 'Error Reading **':VOCID:'** from VOC Table'
            STOP
        END
        DataFileRec=VOC.REC<4>
        DictFileRec=FIELD(VOC.REC<3>,' ',1)
*********************************************************
* VOL.LIST para gets the list of existing NON-VOL files
* and stores VOC ID in an array named NvolVocId
* We check whether the VOC ID selected is already part
* of the NON VOL List, if Yes then Skip , If NOT then
* write it to VOL List, same for Dictionaries only
* unique ones
*********************************************************

        IF LEN(TRIM(DataFileRec)) THEN
            IF DataFileRec[1,1] NE "." AND DataFileRec[1,1] NE "/" THEN         ;* Skip relative/absolute paths
                IF DataFileRec[1,1] NE "\" AND DataFileRec[2,1] NE ":" THEN     ;* skip windows paths
                    FIND VOCID IN NvolVocId SETTING VOL.POS ELSE
                        WRITE DataFileRec TO F.VolatileDataTables,'DATA.':VOCID
                    END
                END
            END
        END

        IF LEN(TRIM(DictFileRec)) THEN
            IF DictFileRec[1,1] NE "." AND DictFileRec[1,1] NE "/" THEN         ;* Skip relative/absolute paths
                IF DictFileRec[1,1] NE "\" AND DictFileRec[2,1] NE ":" THEN     ;* skip windows paths
                    FIND DictFileRec IN TempVolRec SETTING VOL.POS1 ELSE
                        FIND DictFileRec IN TempNonVolatileDictTablesRec SETTING POS5 ELSE
                            WRITE DictFileRec TO F.VolatileDictTables,'DICT.':VOCID
                        END
                    END
                    TempVolRec<-1>=DictFileRec
                END
            END
        END

*********************************************************
        IF LEN(TRIM(DataFileRec)) THEN
            IF DataFileRec[1,1] NE "." AND DataFileRec[1,1] NE "/" THEN         ;* Skip relative/absolute paths
                IF DataFileRec[1,1] NE "\" AND DataFileRec[2,1] NE ":" THEN     ;* skip windows paths
                    WRITE DataFileRec TO F.DataTables,'DATA.':VOCID
                    IF VOCID[-4,4] = "$ARC" THEN
                        WRITE DataFileRec TO F.ArcTables,'DATA.':OCONV(VOCID,"G0$1")
                    END
                END
            END
        END
        IF LEN(TRIM(DictFileRec)) THEN
            IF DictFileRec[1,1] NE "." AND DictFileRec[1,1] NE "/" THEN         ;* Skip relative/absolute paths
                IF DictFileRec[1,1] NE "\" AND DictFileRec[2,1] NE ":" THEN     ;* skip windows paths
                    FIND DictFileRec IN TempDictRec SETTING POS ELSE
                        WRITE DictFileRec TO F.DictTables,'DICT.':VOCID
                    END
                    TempDictRec<-1>=DictFileRec
                END
            END
        END
*
* Get the tablename for the list of T24 files in TabPointers and write  
* on to DF_SEPARATION/Files/TablePointers with ID as DATA.<VOCID>
* These are list of files whose Synonym/Nickname is required to be created
* in Read-Only database.
*
        FIND VOCID IN TabPointers SETTING POS THEN
            Tab.Name = DataFileRec[' ',1,1]
            WRITE Tab.Name TO F.TablePointers,'DATA.':VOCID
        END
        IF NOT(MOD(I,20)) THEN CRT ">":CHAR(0):
        IF NOT(MOD(I,1000)) THEN CRT ">":CHAR(0):' Processed : ':I
        IF I EQ CNT THEN
            IF NOT(MOD(I,20)) THEN CRT ">":CHAR(0):
            IF NOT(MOD(I,CNT)) THEN CRT ">":CHAR(0):'Processed :':I
        END
**********************************************
* Split files according to Company Mnemonic
**********************************************
        IF LEN(TRIM(DataFileRec)) THEN
            FOR X = 1 TO CompCnt
                IF FIELD(VOCID,'.',1)[2,4] EQ FIELD(CompInfo<X>,'-',2) THEN
                    READ CompRelFiles FROM F.CompanyTables,CompInfo<X> THEN

                        CompRelFiles<-1>=VOCID:'*':VOC.REC<4>
                        WRITE CompRelFiles TO F.CompanyTables,CompInfo<X>
                        CompRelFiles = ''
                    END ELSE
                        WRITE VOCID:'*':VOC.REC<4> TO F.CompanyTables,CompInfo<X>
                    END

                END
            NEXT X
        END
*********************************************

        DataFileRec = ''
        DictFileRec = ''


    REPEAT
    CRT ''
    CRT 'T24 Table Information'
    CRT ''
    CRT 'T24 Archive details           :DF_SEPARATION/Files/ArchiveTables'
    CRT 'T24 Company Tables            :DF_SEPARATION/Files/CompanyTables'
    CRT ''
    CRT 'T24 Data Tables               :DF_SEPARATION/Files/DataTables'
    CRT 'T24 Dict Tables               :DF_SEPARATION/Files/DictTables'
    CRT ''
    CRT 'T24 Volatile Data Tables      :DF_SEPARATION/Files/VolatileDataTables'
    CRT 'T24 Volatile Dict Tables      :DF_SEPARATION/Files/VolatileDictTables'
    CRT ''
    CRT 'T24 Non-Volatile Data Tables  :DF_SEPARATION/Files/NonVolatileDataTables'
    CRT 'T24 Non-Volatile Dict Tables  :DF_SEPARATION/Files/NonVolatileDictTables'
    CRT ''
    CRT Hdr
    CRT '     Execute DF.SEPARATION.SCRIPT.GEN to generate scripts '
    CRT Hdr
    CRT ''
    CRT ''

    RETURN
*=====================================================

ARCH.LIST:
*******************************************************
* Read Existing Archive Application get the list of
* Related Files and Main File mentioned in the application
* Store the list of Main and Related file using the
* Same ID as the ARCHIVE Application
*******************************************************
    CLEARSELECT
    CRT "Selecting F.ARCHIVE"
    EXECUTE 'SSELECT F.ARCHIVE' CAPTURING Output

    READLIST ARC.SEL.LIST ELSE ARC.SEL.LIST = ''

    ARC.LIST = ''
    CTRL.LIST = ''
    LOOP
    WHILE READNEXT ARC.ID FROM ARC.SEL.LIST DO

        GOSUB PROCESS.ARCHIVE.ID        ;* Find list of related files and place in FILE.LIST

        FILE.CNT = DCOUNT(MAIN.FILE.LIST,@AM)

        FOR I = 1 TO FILE.CNT

            RelFiles<-1> = MAIN.FILE.LIST<I>:'*':LOWER(FIELD.LIST<I>):'*':CLASS.LIST<I>:"*":SUFFIX.LIST<I>:"*":RO.FILE.LIST<I>

        NEXT I

        WRITE RelFiles TO F.ArchiveTables,ARC.ID
        RelFiles = ''

    REPEAT

*
* Write out control list fields if not already configured
*
    READ temp FROM F.SeparationConfig, 'ControlFields' ELSE
        WRITE CTRL.LIST ON F.SeparationConfig,'ControlFields'
    END
*
* Write out archive information
*
    WRITE ARC.LIST ON F.SeparationFiles,'ArcFields'
    RETURN
*=====================================================

CHECK.FILE.POINTERS:
*******************************************************
* Select the Pointer Files list from FILE.POINTERS 
* and split them by company and store it to TabPointers.
*******************************************************

    FILE.COUNT = DCOUNT(FILE.POINTERS,@FM)
    ERR.TEXT = ''
    FOR FCNT = 1 TO FILE.COUNT
	fileName = FILE.POINTERS<FCNT>
        mainFile = OCONV(fileName,"G0$1")         ;* Get control reference
	
        READ R.FC FROM F.FILE.CONTROL,mainFile ELSE
            errorsRec = 'Error ':mainFile:' missing F.FILE.CONTROL for ':fileName
            CRT errorsRec:" - Skipping entry"
            WRITE errorsRec TO F.Errors,fileName
            CONTINUE          ;* Skip to next file in list
        END
        FILE.TO.OPEN = 'F.':FILE.POINTERS<FCNT>   ;* Prefix with "F."
        *IF FILE.TO.OPEN[4] EQ '$ARC' THEN
            *CALL ARC.OPEN(FILE.TO.OPEN,ERR.TEXT)  ;* Open $ARC files if it is not present then create the $ARC files
            *IF ERR.TEXT THEN  ;* If any error
                *CRT ERR.TEXT  ;* Error message
            *END 
        *END
        *FILE.TO.OPEN<2> = 'NO.FATAL.ERROR'
        *CALL OPF(FILE.TO.OPEN,F.FILE.TO.OPEN)
        *IF ETEXT THEN
            *CRT ETEXT
        *END ELSE
            TabPointers<-1> = FILE.TO.OPEN
        *END
    NEXT FCNT

    RETURN
*=====================================================

COMPANY.INFO:
*******************************************************
* Select the COMPANY Table to get the Company ID and
* MNEMONIC for the company . Store it in an Array
* To be used to Split the NonVolatile Files according
* to company as well as split the whole set of data
* tables based on company
*******************************************************

    CLEARSELECT
    CRT "Selecting F.COMPANY"
    EXECUTE 'SELECT F.COMPANY' CAPTURING Output
    LOOP
        READNEXT CompId ELSE CompId = ''
    WHILE CompId
        READ CompRec FROM F.COMPANY,CompId ELSE
            CRT 'Error Reading ':CompId:' from F.COMPANY Table'
            STOP
        END
        CompInfo<-1>=CompId:'-':CompRec<EB.COM.MNEMONIC>

    REPEAT

    RETURN

*=====================================================

COMP.SPLIT:
*******************************************************
* Select the Archive Files list which is written on to
* DF_SEPARATION/Files/ArchiveTables and split them by company
* and write the list on to DF_SEPARATION/Files/CompanyNonVolatileTables with
* ID as <COMPANY REC.ID>-<MNEMONIC>
*******************************************************
    VOC.REC = '' ; CompRelFiles = '' ; NonCompRelFiles = '' ; X = 0 ; XX = 0
    TempRec = ''
    CompCnt = DCOUNT(CompInfo,@FM)

    EXECUTE 'CLEARSELECT' CAPTURING Output
    EXECUTE 'SELECT ./DF_SEPARATION/Files/ArchiveTables' CAPTURING Output

    LOOP
        READNEXT ArchId ELSE ArchId = ''
    WHILE ArchId

        READ ArchRec FROM F.ArchiveTables,ArchId ELSE
            CRT 'Error Reading ':ArchId:' from ./DF_SEPARATION/Files/ArchiveTables'
            STOP
        END
        RelCnt = DCOUNT(ArchRec,@FM)

        FOR X = 1 TO CompCnt
            CompMne=FIELD(CompInfo<X>,'-',2)
            FOR XX = 1 TO RelCnt
                IF FIELD(ArchRec<XX>,'*',3) EQ 'INT' THEN
                    FIND 'F.':FIELD(ArchRec<XX>,'*',1) IN NonCompRelFiles SETTING V.FLD ELSE
                        READ VOC.REC FROM F.VOC,'F.':FIELD(ArchRec<XX>,'*',1) THEN
                            NonCompRelFiles<-1>=FIELD(ArchRec<XX>,'*',1):'*':'F.':FIELD(ArchRec<XX>,'*',1):'*':VOC.REC<3>:'*':VOC.REC<4>:'*':FIELD(ArchRec<XX>,'*',2)
                        END
                    END
                END ELSE
                    FIND 'F':CompMne:'.':FIELD(ArchRec<XX>,'*',1) IN CompRelFiles SETTING V.FLD ELSE
                        READ VOC.REC FROM F.VOC,'F':CompMne:'.':FIELD(ArchRec<XX>,'*',1) THEN
                            CompRelFiles<-1>=FIELD(ArchRec<XX>,'*',1):'*':'F':CompMne:'.':FIELD(ArchRec<XX>,'*',1):'*':VOC.REC<3>:'*':VOC.REC<4>:'*':FIELD(ArchRec<XX>,'*',2)
                        END
                    END
                END
            NEXT XX
            READ TempRec FROM F.CompanyNonVolatileTables,CompInfo<X> THEN

                CompRelFiles<-1>=TempRec
                WRITE CompRelFiles TO F.CompanyNonVolatileTables,CompInfo<X>
                TempRec = ''
            END ELSE
                WRITE CompRelFiles TO F.CompanyNonVolatileTables,CompInfo<X>
            END
            CompRelFiles = ''

        NEXT X
        WRITE NonCompRelFiles TO F.CompanyNonVolatileTables,'NoCompanyTables'
    REPEAT

*
* Read the list of files whose synonyms has to be created in Read-Only database.
*
    READ FILE.POINTERS FROM F.SeparationConfig, 'CreateRoTablePointers' THEN
        TabPointers = ''
        READ R.SPF.SYSTEM FROM F.SPF,'SYSTEM' THEN
            FOR X=1 TO CompCnt
                CompId = FIELD(CompInfo<X>,'-',1)
                *CALL LOAD.COMPANY(CompId)
                *MAT R.NEW = ''
                GOSUB CHECK.FILE.POINTERS
            NEXT X
        END
    END

    RETURN
*===================================================================
VOL.LIST:
********************************************************************
* Get the list of Non-Vol VOC id's. During the VOC scan phase
* if encountered with a Non-Vol VOC id then skip, as we dont need
* Non-Vol ids in the Vol id list
* Also get the DATA and DICT bit of Non-Vol Files and write
* it in seperate list
********************************************************************

    Id = '' ; J = 0 ; JJ = 0 ; NvolRec = '' ; CNT = 0 ; NvolVocId = ''
    NonVolatileDataTablesRec = '' ; NonVolatileDictTablesRec = '' ; TempNonVolatileDictTablesRec = '' ; NVolId = ''
    DtFld = '' ; DtFldCnt = '' ; TempFd = '' ; DtFldRec = '' ; DtFldPos = ''

    EXECUTE 'CLEARSELECT' CAPTURING Output
    EXECUTE 'SELECT ./DF_SEPARATION/Files/CompanyNonVolatileTables' CAPTURING Output
    LOOP
        READNEXT Id ELSE Id = ''
    WHILE Id
        READ NvolRec FROM F.CompanyNonVolatileTables,Id THEN

            IF NvolRec NE '' THEN
                CNT = DCOUNT(NvolRec,@FM)
                FOR J = 1 TO CNT
                    NvolVocId<-1>=FIELD(NvolRec<J>,'*',2)
                    NVolId=FIELD(NvolRec<J>,'*',2)
                    NonVolatileDataTablesRec=FIELD(NvolRec<J>,'*',4)
                    NonVolatileDictTablesRec=FIELD(FIELD(NvolRec<J>,'*',3),' ',1)
                    NonVolatileDataTablesRec<-1>=FIELD(NvolRec<J>,'*',5)
                    DtFld = FIELD(NvolRec<J>,'*',5)
                    IF DtFld NE '' THEN
                        OPEN 'DICT ':NVolId TO TempFd ELSE
                            ErrorsRec='Error Opening Dict ':NVolId
                            WRITE ErrorsRec TO F.Errors,'DICT.':NVolId
                        END

*                        DtFld = FIELD(NvolRec<J>,'*',5)
                        DtFldCnt = DCOUNT(DtFld,@SM)
                        FOR JJ = 1 TO DtFldCnt

                            IF DtFld<1,1,JJ>[1,1] NE '@' THEN
                                READ DtFldRec FROM TempFd,DtFld<1,1,JJ> ELSE
                                    ErrorsRec='Error reading ':DtFld<1,1,JJ>:' from dictionary of ':NVolId
                                    WRITE ErrorsRec TO F.Errors,DtFld<1,1,JJ>:'.':NVolId
                                END
                                DtFldPos<1,1,JJ>=DtFldRec<2>
                            END ELSE
                                DtFldPos<1,1,JJ>='@'
                            END

                        NEXT JJ
                        NonVolatileDataTablesRec<-1>=DtFldPos
                        CLOSE TempFd
                        JJ = 0 ; TempFd = '' ; DtFld = '' ; DtFldPos = '' ; DtFldRec = ''
                    END

                    WRITE NonVolatileDataTablesRec TO F.NonVolatileDataTables,'DATA.':NVolId
*              WRITE NonVolatileDictTablesRec TO F.NonVolatileDictTables,'DICT.':NVolId
**********************************************************************
* For future use if unique Dicts need to be Isolated
* --------------------------------------------------
                    FIND NonVolatileDictTablesRec IN TempNonVolatileDictTablesRec SETTING NVOL.POS ELSE
                        WRITE NonVolatileDictTablesRec TO F.NonVolatileDictTables,'DICT.':NVolId
                    END
                    TempNonVolatileDictTablesRec<-1>=NonVolatileDictTablesRec
**********************************************************************
                NEXT J
            END
            J = 0 ; CNT = 0 ; NonVolatileDataTablesRec = '' ; NonVolatileDictTablesRec = '' ; NVolId = ''
            JJ = 0 ; TempFd = '' ; DtFld = '' ; DtFldPos = '' ; DtFldRec = ''
        END ELSE
            CRT 'Error Reading ':Id:' from DF_SEPARATION/Files/CompanyNonVolatileTables'
            STOP
        END

    REPEAT

    RETURN

**********************************************************************
PROCESS.ARCHIVE.ID:

    READ R.ARC.REC FROM F.ARCHIVE,ARC.ID ELSE
        CRT "Unable to read record ":ARC.ID:" from F.ARCHIVE"
        STOP
    END

    FILE.LIST = R.ARC.REC<ARC.ARC.FILENAME>
    CONVERT @VM TO @SM IN FILE.LIST
*
* Adjust archive entry for known files associated to archive
*
    BEGIN CASE
    CASE ARC.ID = 'AA.ARRANGEMENT'
        GOSUB FORM.AA.LIST

    CASE ARC.ID = 'MF.ARCHIVAL'
        FILE.LIST = 'TRN.CON.DATE$ARC':@SM:'TRN.CON.TRADE.DATE$ARC':@SM:'TRN.CON.VALUE.DATE$ARC'
        FILE.LIST := @SM:'SECURITY.TRANS$ARC'
        FILE.LIST := @SM:'MF.TRADE$ARC':@SM:'MF.TRADE$NAU$ARC':@SM:'MF.TRADE$HIS$ARC'
        FILE.LIST := @SM:'MF.ORDER$ARC':@SM:'MF.ORDER$NAU$ARC':@SM:'MF.ORDER$HIS$ARC'
        FILE.LIST := @SM:'MF.ORDER.BY.CUST$ARC':@SM:'MF.ORDER.BY.CUST$NAU$ARC':@SM:'MF.ORDER.BY.CUST$HIS$ARC'
        FILE.LIST := @SM:'SEC.TRANS.INFO$ARC'

    CASE ARC.ID = 'SC.SEC.ARCHIVAL'
        FILE.LIST = 'TRN.CON.DATE$ARC':@SM:'TRN.CON.TRADE.DATE$ARC':@SM:'TRN.CON.VALUE.DATE$ARC'
        FILE.LIST := @SM:'SECURITY.TRANS$ARC'
        FILE.LIST := @SM:'SEC.TRADE$ARC':@SM:'SEC.TRADE$NAU$ARC':@SM:'SEC.TRADE$HIS$ARC'
        FILE.LIST := @SM:'SEC.TRADES.TODAY$ARC':@SM:'SEC.TRADE.ADJUST$ARC':@SM:'SEC.TRADE.ENTRIES$ARC':@SM:'SEC.TRADE.ENTRIES.SAVE$ARC':@SM:'SC.BROKER.UNCONF$ARC'
        FILE.LIST := @SM:'SEC.OPEN.ORDER$ARC':@SM:'SEC.OPEN.ORDER$NAU$ARC':@SM:'SEC.OPEN.ORDER$HIS$ARC'       ;* BG_100013261
        FILE.LIST := @SM:'SC.EXE.SEC.ORDERS$ARC':@SM:'SC.EXE.SEC.ORDERS$NAU$ARC':@SM:'SC.EXE.SEC.ORDERS$HIS$ARC'
        FILE.LIST := @SM:'SECURITY.TRANSFER$ARC':@SM:'SECURITY.TRANSFER$NAU$ARC':@SM:'SECURITY.TRANSFER$HIS$ARC'
        FILE.LIST := @SM:'DIARY$ARC':@SM:'DIARY$NAU$ARC':@SM:'DIARY$HIS$ARC'
        FILE.LIST := @SM:'ENTITLEMENT$ARC':@SM:'ENTITLEMENT$NAU$ARC':@SM:'ENTITLEMENT$HIS$ARC'
        FILE.LIST := @SM:'CONCAT.DIARY$ARC':@SM:'ENTITLEMENT.BROKER$ARC':@SM:'DIARY.DATES$ARC'
        FILE.LIST := @SM:'SC.BOOK.COST$ARC':@SM:'SC.BOOK.COST$NAU$ARC':@SM:'SC.BOOK.COST$HIS$ARC'
        FILE.LIST := @SM:'POSITION.TRANSFER$ARC':@SM:'POSITION.TRANSFER$NAU$ARC':@SM:'POSITION.TRANSFER$HIS$ARC'
        FILE.LIST := @SM:'SEC.DEL.CONTROL$ARC':@SM:'SEC.DEL.CONTROL$NAU$ARC':@SM:'SEC.DEL.CONTROL$HIS$ARC':@SM:'SEC.TRANS.INFO$ARC':@SM:'SC.EX.DATE.MTR$ARC'    ;* EN_10003190
        FILE.LIST := @SM:'SC.SETTLEMENT$ARC':@SM:'SC.SETTLEMENT$HIS$ARC'
        FILE.LIST := @SM:'SC.SETT.ENTRIES$ARC':@SM:'SC.SETT.ENTRIES.ORG$ARC':@SM:'SC.SETT.ENTRIES.AUTH$ARC'
        FILE.LIST := @SM:'SC.SETTLE.CUST$ARC'
        FILE.LIST := @SM:'SC.SEC.TRADE.CUST.DETAIL$ARC'     ;* BG_100014262

    CASE ARC.ID = 'SC.INT.ARCHIVAL'
        FILE.LIST = 'SC.PRICE.CHANGE$ARC':@SM:'SC.PRICE.CHANGE.CON$ARC':@SM:'AM.TSDATA$ARC'
    END CASE

*
* Now process FILE.LIST
*

    FILE.CNT = DCOUNT(FILE.LIST,@SM)
    MAIN.FILE.LIST = ''
    RO.FILE.LIST = ''
    FIELD.LIST = ''
    SUFFIX.LIST = ''
    CLASS.LIST = ''
*
* Process list of associated files checking for $HIS versions
*
    I=0
    LOOP
        I++
        fileName = FILE.LIST<1,1,I>     ;* Get next file in list
        fileName = fileName['$',1,COUNT(fileName,'$')] ;* Get actual filename with file.suffix i.e. xxx$HIS/xxx$NAU as defined.
        mainFile = OCONV(fileName,"G0$1")         ;* Get control reference

    WHILE mainFile NE "" DO

*
* Check if file resident in file control
*
        ctrlId = mainFile   ;*Control reference without file.suffix
        READ R.FC FROM F.FILE.CONTROL,mainFile ELSE
            errorsRec = 'Error ':mainFile:' missing F.FILE.CONTROL for ':fileName
            CRT errorsRec:" - Skipping entry"
            WRITE errorsRec TO F.Errors,fileName
            CONTINUE          ;* Skip to next file in list
        END

*
* Check if $HIS to be used or live file
*
        IF fileName[4] NE '$HIS' AND NOT(ARC.ID MATCHES 'AA.ARRANGEMENT':@VM:'DELIVERY.IN':@VM:'DELIVERY.OUT':@VM:'MF.ARCHIVAL':@VM:'SC.SEC.ARCHIVAL') THEN
            LOCATE '$HIS' IN R.FC<EB.FILE.CONTROL.SUFFIXES,1> SETTING V.Val THEN
                LOCATE fileName['$',1,1]:'$HIS$ARC' IN FILE.LIST<1,1,1> SETTING V.Val ELSE
                    fileName = fileName['$',1,1]:'$HIS'
                END
            END
        END

        MAIN.FILE.LIST<-1> = fileName
        RO.FILE.LIST<-1> = fileName:'$RO'
        CLASS.LIST<-1> = R.FC<EB.FILE.CONTROL.CLASS>
        SUFFIX.LIST<-1> = R.FC<EB.FILE.CONTROL.SUFFIXES>
        FIELD.LIST<-1> = R.FC<EB.FILE.DL.DATE.FIELD>

        IF LEN(R.FC<EB.FILE.DL.DATE.FIELD>) THEN
            CTRL.LIST<-1> = ctrlId:@VM:CHANGE(R.FC<EB.FILE.DL.DATE.FIELD>,@VM,@SVM)
        END
    REPEAT

    ARC.LIST<-1> = ARC.ID:@VM:CHANGE(MAIN.FILE.LIST,@AM,@SVM):@VM:CHANGE(RO.FILE.LIST,@AM,@VM):@VM:CHANGE(CHANGE(FIELD.LIST,@VM,@TM),@AM,@SVM)

    RETURN

*
* Special AA processing
*

FORM.AA.LIST:

    SCNT = DCOUNT(FILE.LIST ,@SM)

    LOCATE "AA.ARR.PROPERTY.RECORD$ARC" IN FILE.LIST<1,1,1> SETTING SV.POS THEN
        CLEARSELECT
        EXECUTE 'SELECT F.AA.PROPERTY.CLASS' CAPTURING Output
        READLIST PROPERTY.CLASS ELSE PROPERTY.CLASS = ''
        PROP.CLASS.CNT = DCOUNT(PROPERTY.CLASS,@FM)
        FOR POS=SV.POS TO SCNT-1
            FILE.LIST<1,1,POS+PROP.CLASS.CNT> = FILE.LIST<1,1,POS+1>
        NEXT POS
        FOR I = 1 TO PROP.CLASS.CNT
            FILE.TO.OPEN = "AA.ARR.":PROPERTY.CLASS<I>:"$ARC"
            FILE.LIST<1,1,SV.POS+I-1> = FILE.TO.OPEN
        NEXT I
    END

    SCNT = DCOUNT(FILE.LIST ,@SM)
*Fixed Problems with AA.NEXT.ACTIVITY as the Table was not created and we didn't have concept of reading Product Line in R10 and below releases	
    IF R.SPF.SYSTEM<SPF.CURRENT.RELEASE>[1,3] > 'R10' OR R.SPF.SYSTEM<SPF.CURRENT.RELEASE>[3,2] > '10' THEN
       LOCATE "AA.NEXT.ACTIVITY$ARC" IN FILE.LIST<1,1,1> SETTING SV.POS THEN
          CLEARSELECT
          EXECUTE 'SELECT F.AA.PRODUCT.LINE' CAPTURING Output
          READLIST PRODUCT.LINE ELSE PRODUCT.LINE = ''
          PROD.CNT = DCOUNT(PRODUCT.LINE,@FM)
           FOR POS=SV.POS TO SCNT-1
              FILE.LIST<1,1,POS+PROP.CLASS.CNT> = FILE.LIST<1,1,POS+1>
           NEXT POS
           FOR I = 1 TO PROD.CNT
            FILE.TO.OPEN = "AA.":PRODUCT.LINE<I>:".NEXT.ACTIVITY$ARC"
            FILE.LIST<1,1,SV.POS+I-1> = FILE.TO.OPEN
           NEXT I
       END
    END
    RETURN
*====================================================================
END